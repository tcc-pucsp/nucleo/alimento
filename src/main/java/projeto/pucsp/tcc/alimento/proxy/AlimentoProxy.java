package projeto.pucsp.tcc.alimento.proxy;

import java.time.LocalDateTime;

import projeto.pucsp.tcc.alimento.modelo.TabelaNutricional;

public interface AlimentoProxy {

	Long getCodigo();

	String getNome();

	TabelaNutricional getTabelaNutricional();

	LocalDateTime getDataAtualizacao();
}
