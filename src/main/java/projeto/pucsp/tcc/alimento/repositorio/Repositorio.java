package projeto.pucsp.tcc.alimento.repositorio;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import projeto.pucsp.tcc.alimento.modelo.Alimento;
import projeto.pucsp.tcc.alimento.proxy.AlimentoProxy;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;

public interface Repositorio extends ReactiveMongoRepository<Alimento, Long> {

    Mono<AlimentoProxy> findAlimentoByCodigo(@NotNull Long codigo);

    @Query(value = "{}, {codigo:1, nome: 1, dataAtualizacao:1, tabelaNutricional: 1}")
    Flux<AlimentoProxy> findAllAlimentos();

    Flux<AlimentoProxy> findAllAlimentoByCodigoIn(Iterable<Long> codigos);

}
