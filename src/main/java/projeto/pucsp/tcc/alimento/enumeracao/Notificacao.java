package projeto.pucsp.tcc.alimento.enumeracao;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Notificacao {

	ALIMENTO_NAO_ENCONTRADO(404, "O ALIMENTO REQUISITADO NÃO FOI ENCONTRADO"),
    ALIMENTOS_NAO_ENCONTRADOS(405, "OS ALIMENTOS REQUISITADOS NÃO FORAM ENCONTRADOS");

    private final Integer codigo;

	private final String contexto;

}
