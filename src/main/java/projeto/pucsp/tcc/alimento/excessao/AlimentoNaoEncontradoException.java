package projeto.pucsp.tcc.alimento.excessao;

import java.util.List;

import lombok.Getter;
import projeto.pucsp.tcc.alimento.enumeracao.Notificacao;

public class AlimentoNaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = 931973261372889666L;

	private static final String STATUS_CODIGO_ALIMENTO = "Código do Alimento : <%d>";

	private static final String STATUS_CODIGO_ALIMENTOS = "Código do Alimento : <%s>";

	@Getter
	private final Notificacao protocolo;

	public AlimentoNaoEncontradoException(Long codigo) {

		super(String.format(STATUS_CODIGO_ALIMENTO, codigo));

		this.protocolo = Notificacao.ALIMENTO_NAO_ENCONTRADO;

	}

    public AlimentoNaoEncontradoException(List<Long> codigos) {

		super(String.format(STATUS_CODIGO_ALIMENTOS, codigos.toString()));

		this.protocolo = Notificacao.ALIMENTOS_NAO_ENCONTRADOS;

	}
}
