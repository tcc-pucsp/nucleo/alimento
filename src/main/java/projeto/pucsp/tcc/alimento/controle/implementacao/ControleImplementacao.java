package projeto.pucsp.tcc.alimento.controle.implementacao;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import projeto.pucsp.tcc.alimento.controle.Controle;
import projeto.pucsp.tcc.alimento.proxy.AlimentoProxy;
import projeto.pucsp.tcc.alimento.servico.Servico;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/alimentos")
public class ControleImplementacao implements Controle {

	private final Servico servico;

	public ControleImplementacao(Servico servico) {

		this.servico = servico;

	}

	@GetMapping
	@Override
	public @ResponseBody Flux<AlimentoProxy> obterTodosAlimentos(
			@RequestParam(value = "codigo", required = false) List<Long> codigos) {

		return servico.obterTodosAlimentos(codigos);

	}

	@GetMapping("/{id}")
	@Override
	public @ResponseBody Mono<AlimentoProxy> obterAlimentoPorCodigo(@PathVariable("id") Long codigo) {

		return servico.obterAlimentoPorCodigo(codigo);

	}

}
