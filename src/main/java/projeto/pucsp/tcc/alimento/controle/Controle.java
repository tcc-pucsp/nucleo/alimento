package projeto.pucsp.tcc.alimento.controle;

import projeto.pucsp.tcc.alimento.proxy.AlimentoProxy;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface Controle {

	Flux<AlimentoProxy> obterTodosAlimentos(List<Long> codigos);

	Mono<AlimentoProxy> obterAlimentoPorCodigo(Long codigo);

}
