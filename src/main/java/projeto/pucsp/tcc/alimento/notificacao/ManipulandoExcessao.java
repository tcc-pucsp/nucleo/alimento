package projeto.pucsp.tcc.alimento.notificacao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import projeto.pucsp.tcc.alimento.excessao.AlimentoNaoEncontradoException;
import projeto.pucsp.tcc.alimento.modelo.Resposta;

@RestControllerAdvice
@Slf4j
public class ManipulandoExcessao {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(AlimentoNaoEncontradoException.class)
    public Resposta alimentoNaoEncontrado(AlimentoNaoEncontradoException exception) {

        log.info("[NOTIFICACAO] : EXCEPTION - {}", exception.getProtocolo().getContexto());

        return new Resposta(exception.getProtocolo(), exception.getMessage());

    }

}
