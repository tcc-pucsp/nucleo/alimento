package projeto.pucsp.tcc.alimento.modelo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class TabelaNutricional {

	@NotNull
	private String umidade;

	@NotNull
	@PositiveOrZero
	private String energia;

	@NotNull
	private String proteina;

	@NotNull
	private String lipideos;

	@NotBlank
	private String colesterol;

	@NotNull
	private String carboidratos;

	@NotNull
	private String fibraAlimentar;

	@NotNull
	@PositiveOrZero
	private String calcio;

	@NotNull
	@PositiveOrZero
	private String magnesio;

	@NotNull
	@PositiveOrZero
	private String fosforo;

	@NotNull
	private String ferro;

	@NotNull
	@PositiveOrZero
	private String sodio;

	@NotNull
	@PositiveOrZero
	private String potassio;

	@NotNull
	private String cobre;

	@NotNull
	private String zinco;

}
