package projeto.pucsp.tcc.alimento.modelo;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document(collection = "alimentos")
@Data
public class Alimento {

	@Id
	private ObjectId id;

	@NotNull
	private Long codigo;

	@NotBlank
	private String nome;

	@LastModifiedDate
	private LocalDateTime dataCadastro;

	@LastModifiedDate
	private LocalDateTime dataAtualizacao;

	@NotNull
	private TabelaNutricional tabelaNutricional;

}
