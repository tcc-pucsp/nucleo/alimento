package projeto.pucsp.tcc.alimento.modelo;

import lombok.Value;
import projeto.pucsp.tcc.alimento.enumeracao.Notificacao;

@Value
public class Resposta {

	Notificacao notificacao;

	String mensagem;

}
