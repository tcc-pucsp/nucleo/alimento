package projeto.pucsp.tcc.alimento.servico.implementacao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.alimento.excessao.AlimentoNaoEncontradoException;
import projeto.pucsp.tcc.alimento.proxy.AlimentoProxy;
import projeto.pucsp.tcc.alimento.repositorio.Repositorio;
import projeto.pucsp.tcc.alimento.servico.Servico;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Service
public class PesquisarAlimentos implements Servico {

    private final Repositorio repositorio;

    public PesquisarAlimentos(Repositorio repositorio) {

        this.repositorio = repositorio;

    }

    @Override
    public Flux<AlimentoProxy> obterTodosAlimentos(List<Long> codigos) {

        if (codigos != null) {

            log.info("[SERVICO] : Obtendo todos os Alimentos para codigos {}", codigos);

            Flux<AlimentoProxy> alimentos = repositorio.findAllAlimentoByCodigoIn(codigos);

            return alimentos
                    .switchIfEmpty(s -> s.onError(new AlimentoNaoEncontradoException(codigos)));

        }

        log.info("[SERVICO] : Obtendo todos os Alimentos");

        return repositorio.findAllAlimentos();

    }

    @Override
    public Mono<AlimentoProxy> obterAlimentoPorCodigo(Long codigo) {

        log.info("[SERVICO] : Obtendo Alimento com ID[{}]", codigo);

        return repositorio.findAlimentoByCodigo(codigo)
                .switchIfEmpty(Mono.error(new AlimentoNaoEncontradoException(codigo)));

    }

}
