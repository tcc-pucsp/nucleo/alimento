package projeto.pucsp.tcc.alimento.controle;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import projeto.pucsp.tcc.alimento.controle.implementacao.ControleImplementacao;
import projeto.pucsp.tcc.alimento.modelo.TabelaNutricional;
import projeto.pucsp.tcc.alimento.proxy.AlimentoProxy;
import projeto.pucsp.tcc.alimento.repositorio.Repositorio;
import projeto.pucsp.tcc.alimento.servico.Servico;
import projeto.pucsp.tcc.alimento.servico.implementacao.PesquisarAlimentos;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@ActiveProfiles(value = "test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ControleTest {

    private static final LocalDateTime NOW = LocalDateTime.now();

    private Controle controle;

    @Autowired
    private ApplicationContext context;

    @Mock
    private Repositorio repositorio;

    private WebTestClient client;

    @Before
    public void setup() {

        Servico servico = new PesquisarAlimentos(repositorio);

        controle = new ControleImplementacao(servico);

        client = WebTestClient.bindToApplicationContext(this.context).configureClient().build();

    }

    @Test
    public void obterNenhumAlimentoTest() {

        client
                .get()
                .uri(URI.create("/alimentos"))
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody()
                .json("[]");

    }

    @Test
    public void obterTodosAlimentoTest() {

        Mockito.when(repositorio.findAllAlimentos()).thenReturn(Flux.just(obterAlimentosProxy()));

        Flux<AlimentoProxy> alimentos = controle.obterTodosAlimentos(null);

        Long quantidade = alimentos.count().block();

        Assert.assertSame("quantidade deve ser igual a 1", 1L, quantidade);

    }

    @Test
    public void obterTodosAlimentoPorListIdTest() {

        final List<Long> codigos = Collections.singletonList(1L);

        Mockito.when(repositorio.findAllAlimentos()).thenReturn(Flux.just(obterAlimentosProxy()));

        Mockito.when(repositorio.findAllAlimentoByCodigoIn(codigos))
                .thenReturn(Flux.just(obterAlimentosProxy()));

        Flux<AlimentoProxy> alimentosFlux = controle.obterTodosAlimentos(codigos);

        List<AlimentoProxy> alimentos = alimentosFlux.collectList().block();

        Assert.assertNotNull("alimentos não deve ser nulo", alimentos);
        Assert.assertEquals("quantidade deve ser igual a 1", 1L, alimentos.size());
        Assert.assertSame("deve retornar alimento com id 1", 1L, alimentos.get(0).getCodigo());

    }

    @Test
    public void obterTodosAlimentoPorListIdParaIdInexistenteTest() {

        client
                .get()
                .uri(URI.create("/alimentos?codigo=0,1"))
                .exchange()
                .expectStatus()
                .is4xxClientError()
                .expectBody()
                .jsonPath("$.notificacao").isNotEmpty()
                .jsonPath("$.notificacao.codigo").isEqualTo(405);
    }

    @Test
    public void obterAlimentoPorCodigoCadastradoTest() {

        Long codigoAlimento = 1L;

        AlimentoProxy alimentosProxy = obterAlimentosProxy();

        Mockito.when(repositorio.findAlimentoByCodigo(codigoAlimento)).thenReturn(Mono.just(alimentosProxy));

        Mono<AlimentoProxy> alimentoMono = controle.obterAlimentoPorCodigo(codigoAlimento);

        AlimentoProxy alimento = alimentoMono.block();

        Assert.assertNotNull("alimento não deve ser nulo", alimento);
        Assert.assertSame("alimento deve ter codigo 1", codigoAlimento, alimento.getCodigo());
        Assert.assertEquals("alimento deve ter nom", "ALIMENTO", alimento.getNome());
        Assert.assertNotNull("alimento deve ter tabela nutricional nao nula", alimento.getTabelaNutricional());
        Assert.assertEquals("alimento deve ter data de atualizacao igual", alimentosProxy.getDataAtualizacao(),
                alimento.getDataAtualizacao());

    }

    @Test
    public void obterAlimentoPorCodigoNaoCadastradoTest() {

        Integer codigo = 1;

        client
                .get()
                .uri(URI.create(String.format("/alimentos/%d", codigo)))
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody()
                .jsonPath("$.notificacao").isNotEmpty()
                .jsonPath("$.notificacao.codigo").isEqualTo(404);

    }

    private AlimentoProxy obterAlimentosProxy() {

        return new AlimentoProxy() {

            @Override
            public TabelaNutricional getTabelaNutricional() {
                return new TabelaNutricional();
            }

            @Override
            public String getNome() {
                return "ALIMENTO";
            }

            @Override
            public Long getCodigo() {
                return 1L;
            }

            @Override
            public LocalDateTime getDataAtualizacao() {
                return NOW;
            }

        };
    }

}
